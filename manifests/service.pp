class ethminer::service inherits ethminer {

  file { '/lib/systemd/system/ethminer.service':
    ensure  => file,
    content => template('ethminer/ethminer.service.erb'),
    notify  => Exec['ethminer:daemon-reload']
  }

  exec {'ethminer:daemon-reload':
    command     => '/bin/systemctl daemon-reload',
    refreshonly => true,
    notify      => Service['ethminer']
  }

  service { 'ethminer':
    ensure => $ethminer::enabled ? {
      true  => running,
      false => stopped,
    },
    enable => $ethminer::enabled
  }
}